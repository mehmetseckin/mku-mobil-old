package com.mehmetseckin.mku.mobil;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

import com.mehmetseckin.mku.mobil.readers.RSSReader;
import com.mehmetseckin.mku.mobil.util.RSSArticle;

public class SplashScreen extends Activity {
	protected ProgressDialog pDialog;
	protected boolean _splashActive = true;
	protected Intent data;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		pDialog = new ProgressDialog(this);
		data = new Intent();
		setResult(RESULT_OK, data);
		// Show splash screen & then initialize some user login info & gui stuff
		showSplashScreen();
		new InitializeLists().execute();
	}
	@Override
	protected void onStart() {
		super.onStart();
		if(!isNetworkConnected()) {
			Log.d("SplashActivity",	"No internet connection.");
			Main.alertNoNetwork = true;
		}
	}
	private void showSplashScreen() {
		setContentView(R.layout.splash_screen);
		Thread splashThread = new Thread() {
			int waited = 0;
			@Override
			public void run() {
				try {
					waited = 0;
					while (_splashActive) {
						sleep(100);
						if (_splashActive)
							// If still active, add 100 ms to waited time.
							waited += 100;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
					runOnUiThread(new Runnable() {
						
						public void run() {
							Log.d("SplashScreen", "Duration: " + waited + " ms");
							killSplashScreen();
						}
					});
				}
			}
		};
		splashThread.start();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			setResult(RESULT_CANCELED);
			killSplashScreen();
		}
		return true;
	}
	
	protected void killSplashScreen() {
		_splashActive = false;
		this.finish();
	}
	class InitializeLists extends AsyncTask<Void, Void, Boolean> {
		List<RSSArticle> news, announcements, events, docs;
		{ news = announcements = events = docs = new ArrayList<RSSArticle>();}				// Initialize data lists.
		@Override
		protected Boolean doInBackground(Void... arg0) {
			try {
				news = RSSReader.getLatestRssFeed(Settings.NEWS_FEED_URL);
				announcements = RSSReader.getLatestRssFeed(Settings.ANNO_FEED_URL);
				events = RSSReader.getLatestRssFeed(Settings.EVENTS_FEED_URL);
				docs = RSSReader.getLatestRssFeed(Settings.DOCS_FEED_URL);
			} catch (Exception e) {
				Log.e("RSS ERROR", "Error loading RSS Feed Stream >> " + e.getMessage() + " //" + e.toString());
				return false;
			}
			return true;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			Main.news = this.news;
			Main.announcements = this.announcements;
			Main.events = this.events;
			Main.docs = this.docs;
			if(result) {
				killSplashScreen();
			}
			else {
				Main.alertNoNetwork = true;
			}
		}
		
	}
    class InitializeNewsList extends AsyncTask<Void, Void, List<RSSArticle>> {    	
    	
		@Override
		protected List<RSSArticle> doInBackground(Void... params) {
			List<RSSArticle> news = new ArrayList<RSSArticle>();
			try {
				news = RSSReader.getLatestRssFeed(Settings.NEWS_FEED_URL);
			} catch (Exception e) {
				Log.e("RSS ERROR", "Error loading RSS Feed Stream >> " + e.getMessage() + " //" + e.toString());
			}
			return news;
		}
		
		@Override
		protected void onPostExecute(List<RSSArticle> result) {
			super.onPostExecute(result);
			if(pDialog.isShowing()) pDialog.dismiss();
			Main.news = result;
		}
    	
    }
    class InitializeAnnouncementsList extends AsyncTask<Void, Void, List<RSSArticle>> {    	
    	
		@Override
		protected List<RSSArticle> doInBackground(Void... params) {
			List<RSSArticle> announcements = new ArrayList<RSSArticle>();
			try {
				announcements = RSSReader.getLatestRssFeed(Settings.ANNO_FEED_URL);
			} catch (Exception e) {
				Log.e("RSS ERROR", "Error loading RSS Feed Stream >> " + e.getMessage() + " //" + e.toString());
			}
			return announcements;
		}
		
		@Override
		protected void onPostExecute(List<RSSArticle> result) {
			super.onPostExecute(result);
			if(pDialog.isShowing()) pDialog.dismiss();
			Main.announcements = result;
		}
    	
    }
    class InitializeEventsList extends AsyncTask<Void, Void, List<RSSArticle>> {    	
    	
		@Override
		protected List<RSSArticle> doInBackground(Void... params) {
			List<RSSArticle> events = new ArrayList<RSSArticle>();
			try {
				events = RSSReader.getLatestRssFeed(Settings.EVENTS_FEED_URL);
			} catch (Exception e) {
				Log.e("RSS ERROR", "Error loading RSS Feed Stream >> " + e.getMessage() + " //" + e.toString());
			}
			return events;
		}
		
		@Override
		protected void onPostExecute(List<RSSArticle> result) {
			super.onPostExecute(result);
			if(pDialog.isShowing()) pDialog.dismiss();
			Main.events = result;
		}
    	
    }
    class InitializeDocsList extends AsyncTask<Void, Void, List<RSSArticle>> {    	
    	
		@Override
		protected List<RSSArticle> doInBackground(Void... params) {
			List<RSSArticle> docs = new ArrayList<RSSArticle>();
			try {
				docs = RSSReader.getLatestRssFeed(Settings.DOCS_FEED_URL);
			} catch (Exception e) {
				Log.e("RSS ERROR", "Error loading RSS Feed Stream >> " + e.getMessage() + " //" + e.toString());
			}
			return docs;
		}
		
		@Override
		protected void onPostExecute(List<RSSArticle> result) {
			super.onPostExecute(result);
			if(pDialog.isShowing()) pDialog.dismiss();
			Main.docs = result;
		}
    	
    }
	private boolean isNetworkConnected() {
		  ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo ni[] = cm.getAllNetworkInfo();
		  for(NetworkInfo n : ni) {
			  if(n.getState() == State.CONNECTED)
				  return true;
		  }
		  Log.d("isNetworkConnected","NO!");
		  return false;
	}
}
