package com.mehmetseckin.mku.mobil.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.mehmetseckin.mku.mobil.Settings;

public class SPLoader {
	private String PREFS_NAME = "com.mehmetseckin.mku.mobil";
	private Activity mActivity;
	private SharedPreferences.Editor editor;
	private SharedPreferences settings;
	private static class Keys {
		static String AUTO_REFRESH = "autoRefresh";
		static String AUTO_REFRESH_TIME_INTERVAL = "autoRefreshTimeInterval";
		static String MAX_ITEMS = "maxItems"; 
	}
	public SPLoader(Activity activity) {
		mActivity = activity;
		settings = mActivity.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE);
		editor = settings.edit();
	}
	
	public void save() {
		Log.d("SPLoader", "Saving settings...");
		editor.putBoolean(Keys.AUTO_REFRESH, Settings.AUTO_REFRESH);
		editor.putInt(Keys.AUTO_REFRESH_TIME_INTERVAL, Settings.AUTO_REFRESH_TIME_INTERVAL);
		editor.putInt(Keys.MAX_ITEMS, Settings.MAX_ITEMS);
		editor.commit();
		Log.d("SPLoader", "Done...");
	}
	
	public void load() {
		Log.d("SPLoader", "Loading settings...");
		Settings.AUTO_REFRESH = settings.getBoolean(Keys.AUTO_REFRESH, false);
		Settings.AUTO_REFRESH_TIME_INTERVAL = settings.getInt(Keys.AUTO_REFRESH_TIME_INTERVAL, 10);
		Settings.MAX_ITEMS = settings.getInt(Keys.MAX_ITEMS, 15);
		Log.d("SPLoader", "Done...");
	}

}
