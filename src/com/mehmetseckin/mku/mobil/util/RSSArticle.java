package com.mehmetseckin.mku.mobil.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mehmetseckin.mku.mobil.Settings;

public class RSSArticle {
	private String title;
	private String description;
	private String imgLink;
	private Bitmap img;
	private URL url;
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * Eger aciklama resim iceriyorsa, resmin linkini imgLink icine kaydet.
	 * resmi de bitmap objesine kaydet.
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
		if (description.contains("<img ")){
			String img  = description.substring(description.indexOf("<img "));
			String cleanUp = img.substring(0, img.indexOf(">")+1);
			img = img.substring(img.indexOf("src=") + 5);
			int indexOf = img.indexOf("'");
			if (indexOf==-1){
				indexOf = img.indexOf("\"");
			}
			img = img.substring(0, indexOf);
			setImgLink(img);
    		String url = getImgLink();
            URL feedImage;
            Bitmap imgb = null;
            try {
				feedImage = new URL(Settings.MAIN_SITE_URL + "/" + url);        	
	        	HttpURLConnection conn= (HttpURLConnection)feedImage.openConnection();
	            InputStream is = conn.getInputStream();
	            imgb = BitmapFactory.decodeStream(is);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
            setImg(imgb);
            this.description = this.description.replace(cleanUp, "");
		}
	}
	/**
	 * @return the url
	 */
	public URL getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(URL url) {
		this.url = url;
	}
	/**
	 * @return the imgLink
	 */
	public String getImgLink() {
		return imgLink;
	}
	/**
	 * @param imgLink the imgLink to set
	 */
	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}
	/**
	 * @return the img
	 */
	public Bitmap getImg() {
		return img;
	}
	/**
	 * @param img the img to set
	 */
	public void setImg(Bitmap img) {
		this.img = img;
	}
}
