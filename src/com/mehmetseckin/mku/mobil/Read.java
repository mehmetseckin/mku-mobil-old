package com.mehmetseckin.mku.mobil;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class Read extends Activity {
	TextView txtTitle, txtDescription;
	ImageView imgThumb, imgBack;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_read);
		txtTitle = (TextView) findViewById(R.id.txtTitle);
		txtTitle.setText(getIntent().getExtras().getString("title"));
		txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, (float)18.6);
		txtDescription = (TextView) findViewById(R.id.txtDescription);
		txtDescription.setText(Html.fromHtml(getIntent().getExtras().getString("description")));
		imgThumb = (ImageView) findViewById(R.id.imgThumb);
        URL feedImage;
        Bitmap imgb = null;
        if(getIntent().getExtras().getString("imgUrl")!=null) {
	        try {
				feedImage = new URL(Settings.MAIN_SITE_URL + "/" + getIntent().getExtras().getString("imgUrl"));        	
				imgb = new GetBitmap().execute(feedImage).get();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		if(imgb!=null) imgThumb.setImageBitmap(imgb);
		
		imgBack = (ImageView) findViewById(R.id.imgBack);
		imgBack.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Read.this.finish();
			}
		});
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}
	
	class GetBitmap extends AsyncTask<URL, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(URL... urls) {
			URL feedImage = urls[0];
        	HttpURLConnection conn;
			try {
				conn = (HttpURLConnection)feedImage.openConnection();           
				InputStream is = conn.getInputStream();
	            return BitmapFactory.decodeStream(is);
			} catch (IOException e) {
				Log.d("Read", "IOException while getting the bitmap");
				return null;
			}
		}
		
	}

}
