package com.mehmetseckin.mku.mobil;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.mehmetseckin.mku.mobil.util.SPLoader;

public class Options extends Activity {

	private boolean CHANGED = false;
	
	SeekBar sbMaxItems, sbTimeInterval;
	CheckBox cbAutoRefresh;
	TextView txtMaxItems, txtTimeInterval, txtInfo;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        initialize();
    }
    
    private void initialize() {
    	(new SPLoader(this)).load();
        sbMaxItems = (SeekBar) findViewById(R.id.sbMaxItems);
        sbTimeInterval = (SeekBar) findViewById(R.id.sbTimeInterval);
        cbAutoRefresh = (CheckBox) findViewById(R.id.cbAutoRefresh);
        txtMaxItems = (TextView) findViewById(R.id.txtMaxItems);
        txtTimeInterval = (TextView) findViewById(R.id.txtTimeInterval);
        txtInfo = (TextView) findViewById(R.id.txtInfo);
        
        txtInfo.setText("");
        txtInfo.setVisibility(View.INVISIBLE);
        
        sbMaxItems.setMax(100);
        sbMaxItems.setProgress(Settings.MAX_ITEMS);
        sbMaxItems.setFocusable(true);
        txtMaxItems.setText("Yüklenecek maksimum eleman sayısı : " + sbMaxItems.getProgress());
        sbMaxItems.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				txtInfo.setVisibility(View.INVISIBLE);
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				txtInfo.setVisibility(View.VISIBLE);
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				txtInfo.setText(String.valueOf(progress));
				txtMaxItems.setText("Yüklenecek maksimum eleman sayısı : " + progress);
				Settings.MAX_ITEMS = progress;
				CHANGED = true;
			}
		});
        
        sbTimeInterval.setMax(120);
        sbTimeInterval.setProgress(Settings.AUTO_REFRESH_TIME_INTERVAL);
        txtTimeInterval.setText("Veriler, " + sbTimeInterval.getProgress() + " saniyelik aralıklarla güncellenecek.");
        sbTimeInterval.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				txtInfo.setVisibility(View.INVISIBLE);
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				txtInfo.setVisibility(View.VISIBLE);
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(progress < 15) progress = 15;
				txtInfo.setText(String.valueOf(progress));
				txtTimeInterval.setText("Veriler, " + progress + " saniyelik aralıklarla güncellenecek.");
				Settings.AUTO_REFRESH_TIME_INTERVAL = progress;
				CHANGED = true;
			}
		});
        cbAutoRefresh.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				Settings.AUTO_REFRESH = isChecked;
				CHANGED = true;
				if(isChecked) {
		        	sbTimeInterval.setVisibility(View.VISIBLE);
		        	txtTimeInterval.setVisibility(View.VISIBLE);
				}
				else {
		        	sbTimeInterval.setVisibility(View.INVISIBLE);
		        	txtTimeInterval.setVisibility(View.INVISIBLE);
				}
			}
		});
        cbAutoRefresh.setChecked(Settings.AUTO_REFRESH);
		if(cbAutoRefresh.isChecked()) {
        	sbTimeInterval.setVisibility(View.VISIBLE);
        	txtTimeInterval.setVisibility(View.VISIBLE);
		}
		else {
        	sbTimeInterval.setVisibility(View.INVISIBLE);
        	txtTimeInterval.setVisibility(View.INVISIBLE);
		}
        
    }
    @Override
    protected void onPause() {
    	super.onPause();
    	if(CHANGED) {
	    	(new SPLoader(this)).save();
	    	Log.d("Options", "Settings saved.");
	    	Toast.makeText(Options.this, "Ayarlarınız kaydedildi.", Toast.LENGTH_SHORT).show();
    	}
    }
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	(new SPLoader(this)).load();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_options, menu);
        return true;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
