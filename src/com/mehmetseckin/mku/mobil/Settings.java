package com.mehmetseckin.mku.mobil;

public class Settings {
	public static final String MAIN_SITE_URL = "http://www.mku.edu.tr";
	public static final String MAIL_URL = "https://posta.mku.edu.tr/mail/";
	public static final String NEWS_FEED_URL = "http://www.mku.edu.tr/feed.php?lang=tr&type=news";
	public static final String ANNO_FEED_URL = "http://www.mku.edu.tr/feed.php?lang=tr&type=ann";
	public static final String EVENTS_FEED_URL = "http://www.mku.edu.tr/feed.php?lang=tr&type=events";
	public static final String DOCS_FEED_URL = "http://www.mku.edu.tr/feed.php?lang=tr&type=docs";

	public static final String ANNO_MOBILE_ARCHIVE_URL = "http://mku.edu.tr/mobil/list.php?type=d";
	public static final String EVENTS_MOBILE_ARCHIVE_URL = "http://mku.edu.tr/mobil/list.php?type=e";
	
	public static boolean AUTO_REFRESH = false;
	public static int AUTO_REFRESH_TIME_INTERVAL = 0;
	public static int MAX_ITEMS = 5;

}
