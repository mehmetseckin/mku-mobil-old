package com.mehmetseckin.mku.mobil;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;

import com.mehmetseckin.mku.mobil.adapters.RSSListAdapter;
import com.mehmetseckin.mku.mobil.readers.RSSReader;
import com.mehmetseckin.mku.mobil.util.RSSArticle;
import com.mehmetseckin.mku.mobil.util.SPLoader;
public class Main extends Activity {
	public static boolean alertNoNetwork = false; 									// Internet connection flag.
	public static List<RSSArticle> news, announcements, events, docs;					// Lists for collecting data.
	{ news = announcements = events = docs = new ArrayList<RSSArticle>();}				// Initialize data lists.
	public final int REQUEST_CODE_SPLASH = 10;										// SPLASH SCREEN Specific request code
	
	TabHost tabHost;
	TabSpec tHome, tNews, tAnnouncements, tEvents, tDocs;
	ListView lNews, lAnnouncements, lEvents, lDocs;
	ImageButton btnSettings, btnRefresh, btnMail;
	
	protected ProgressDialog pDialog, downloadDialog;
	protected AlertDialog noNetworkDialog;
	
	private Handler handler = new Handler();
	private Timer refresherDaemon;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("Main","<!-- onCreate callback -->");
        /* Load settings */
        (new SPLoader(this)).load();
        
        /* 	Initialize dialogs... */
		pDialog = new ProgressDialog(this);
		noNetworkDialog = new AlertDialog.Builder(this)
				.setTitle("Eyvah!")
				.setMessage("Internet baglantisini saglayamadik. L�tfen baglantinizi kontrol edin ve tekrar deneyin.")
				.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
			}
		}).create();
		/*	Initializing dialogs complete... */
		
		/*	Get main content view	*/
        setContentView(R.layout.activity_main);

        /* Start splash-screen activity and initialize lists.	*/
		showSplashScreen();

        /*	Initialize tabs and stuff... */
        initialize();
    }
	@Override
	protected void onResume() {
		super.onResume();
        Log.i("Main","<!-- onResume callback -->");
		if(news.isEmpty() || announcements.isEmpty() || events.isEmpty() || docs.isEmpty())
			new RefreshLists().execute();
		handleRefresherDaemon();
	}
	@Override
	protected void onRestart() {
		super.onRestart();
        Log.i("Main","<!-- onRestart callback -->");
        (new SPLoader(this)).load();
		handleRefresherDaemon();
	}
    @Override
    protected void onStart() {
    	super.onStart();
        Log.i("Main","<!-- onStart callback -->");
		handleRefresherDaemon();

    }
    @Override
    protected void onPause() {
    	super.onPause();
        Log.i("Main","<!-- onPause callback -->");
    }
    @Override
    protected void onDestroy() {
    	super.onDestroy();
        Log.i("Main","<!-- onDestroy callback -->");
    }
    protected void initialize() {
        /* Init tabs... */
        tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();
        
        tHome = tabHost.newTabSpec("Ev");
        tHome.setContent(R.id.tab_home);
        tHome.setIndicator("Ev", getResources().getDrawable(R.drawable.ic_home));
        
        tNews = tabHost.newTabSpec("Haberler");
        tNews.setContent(R.id.tab_news);
        tNews.setIndicator("Haberler", getResources().getDrawable(R.drawable.ic_news));
        
        tAnnouncements = tabHost.newTabSpec("Duyurular");
        tAnnouncements.setContent(R.id.tab_announcements);
        tAnnouncements.setIndicator("Duyurular", getResources().getDrawable(R.drawable.ic_announcements));
        
        tEvents = tabHost.newTabSpec("Etkinlikler");
        tEvents.setContent(R.id.tab_events);
        tEvents.setIndicator("Etkinlikler", getResources().getDrawable(R.drawable.ic_events));
        
        tDocs = tabHost.newTabSpec("Belgeler");
        tDocs.setContent(R.id.tab_docs);
        tDocs.setIndicator("Belgeler", getResources().getDrawable(R.drawable.ic_documents));  
        
        tabHost.addTab(tHome);
        tabHost.addTab(tNews);
        tabHost.addTab(tAnnouncements);
        tabHost.addTab(tEvents);
        tabHost.addTab(tDocs);
        tabHost.setOnTabChangedListener(new OnTabChangeListener() {
			
			public void onTabChanged(String tabId) {
				String currentTag = tabHost.getCurrentTabTag();
				if(currentTag.equalsIgnoreCase(tNews.getTag())) {
					Log.d("Main", "Initializing news tab...");
					initializeNewsTab();				
				}
				if(currentTag.equalsIgnoreCase(tAnnouncements.getTag())) {
					Log.d("Main", "Initializing announcements tab...");
					initializeAnnouncementsTab();				
				}
				if(currentTag.equalsIgnoreCase(tEvents.getTag())) {
					Log.d("Main", "Initializing events tab...");
					initializeEventsTab();				
				}
				if(currentTag.equalsIgnoreCase(tDocs.getTag())) {
					Log.d("Main", "Initializing docs tab...");
					initializeDocsTab();				
				}
			}
		});
        tabHost.setCurrentTab(0);
        /*** Init tabs complete ***/
        
        /* Init buttons */
        btnSettings = (ImageButton) findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				startActivity(new Intent(Main.this, Options.class));
			}
		});
        
        btnRefresh = (ImageButton) findViewById(R.id.btnRefresh);
        btnRefresh.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				new RefreshLists().execute();
			}
		});
        
        btnMail = (ImageButton) findViewById(R.id.btnMail);
        btnMail.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Settings.MAIL_URL)));
			}
		});
        /* Init buttons completed. */
    }
    protected void initializeNewsTab() {
        lNews = (ListView) findViewById(R.id.list_news);
        RSSListAdapter adapter = new RSSListAdapter(this, news, R.layout.item_news_list, R.id.thumb_news, R.id.title_news);
        lNews.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long arg3) {
				Log.d("Main", "News selected: " + position);	
				RSSArticle selected = (RSSArticle)adapter.getItemAtPosition(position);
				Intent read = new Intent(Main.this, Read.class);
				read.putExtra("title", selected.getTitle());
				read.putExtra("description", selected.getDescription());
				read.putExtra("imgUrl", selected.getImgLink());
				Log.d("Main", "Launching read activity : imgUrl: "+selected.getImgLink());
				startActivity(read);
			}
		});
        lNews.setAdapter(adapter);
    }
    protected void initializeAnnouncementsTab() {
    	lAnnouncements = (ListView) findViewById(R.id.list_announcements);
        RSSListAdapter adapter = new RSSListAdapter(this, announcements, R.layout.item_announcements_list, R.id.thumb_announcements, R.id.title_announcements);
        lAnnouncements.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long arg3) {
				Log.d("Main", "Announcement selected: " + position);
//				Intent announcements = new Intent(Intent.ACTION_VIEW, 
//						Uri.parse(Settings.ANNO_MOBILE_ARCHIVE_URL)
//						);
//				Log.d("Main","Launching browser to : " + ((Article) adapter.getItemAtPosition(position)).getUrl().toString());
//				startActivity(announcements);
				RSSArticle selected = (RSSArticle)adapter.getItemAtPosition(position);
				Intent read = new Intent(Main.this, Read.class);
				read.putExtra("title", selected.getTitle());
				read.putExtra("description", selected.getDescription());
				read.putExtra("imgUrl", selected.getImgLink());
				Log.d("Main", "Launching read activity : imgUrl: "+selected.getImgLink());
				startActivity(read);
			}
		});
        lAnnouncements.setAdapter(adapter);

    }
    protected void initializeEventsTab() {
        lEvents = (ListView) findViewById(R.id.list_events);
        RSSListAdapter adapter = new RSSListAdapter(this, events, R.layout.item_events_list, R.id.thumb_events, R.id.title_events);
        lEvents.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long arg3) {
				Log.d("Main", "Event selected: " + position);
//				Intent events = new Intent(Intent.ACTION_VIEW, 
//						Uri.parse(Settings.EVENTS_MOBILE_ARCHIVE_URL)
//						);
//				Log.d("Main","Launching browser to : " + ((Article) adapter.getItemAtPosition(position)).getUrl().toString());
//				startActivity(events);
				RSSArticle selected = (RSSArticle)adapter.getItemAtPosition(position);
				Intent read = new Intent(Main.this, Read.class);
				read.putExtra("title", selected.getTitle());
				read.putExtra("description", selected.getDescription());
				read.putExtra("imgUrl", selected.getImgLink());
				Log.d("Main", "Launching read activity : imgUrl: "+selected.getImgLink());
				startActivity(read);
			}
		});
        lEvents.setAdapter(adapter);

    }
    protected void initializeDocsTab() {
        lDocs = (ListView) findViewById(R.id.list_docs);
        RSSListAdapter adapter = new RSSListAdapter(this, docs, R.layout.item_docs_list, R.id.thumb_docs, R.id.title_docs);
        lDocs.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long arg3) {
				Log.d("Main", "Document selected: " + position);				
				Intent docs = new Intent(Intent.ACTION_VIEW, 
						Uri.parse(
								((RSSArticle)adapter.getItemAtPosition(position))
								.getUrl()
								.toString())
						);
				Log.d("Main","Launching browser to : " + ((RSSArticle) adapter.getItemAtPosition(position)).getUrl().toString());
				startActivity(docs);
			}
		});
        lDocs.setAdapter(adapter);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(Main.this, Options.class));
                return true;
            case R.id.menu_update:
                new RefreshLists().execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	private void showSplashScreen() {
			startActivityForResult(new Intent(this, SplashScreen.class), REQUEST_CODE_SPLASH);
	}
	private class RefreshLists extends AsyncTask<Void, Integer, Boolean> {
		protected void onPreExecute() {
			pDialog.setTitle("L�tfen bekleyiniz...");
			pDialog.setMessage("Bilgiler g�ncelleniyor...");
			pDialog.show();
		}
		@Override
		protected Boolean doInBackground(Void... params) {
			Main.news = RSSReader.getLatestRssFeed(Settings.NEWS_FEED_URL);
			Main.announcements = RSSReader.getLatestRssFeed(Settings.ANNO_FEED_URL);
			Main.events = RSSReader.getLatestRssFeed(Settings.EVENTS_FEED_URL);
			Main.docs = RSSReader.getLatestRssFeed(Settings.DOCS_FEED_URL);
			if(news.isEmpty() || announcements.isEmpty() || events.isEmpty() || docs.isEmpty())
				return false;
			return true;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(pDialog.isShowing()) pDialog.dismiss();
			if(noNetworkDialog.isShowing()) noNetworkDialog.dismiss();
			if(!result) {
				Log.d("Main.RefreshList","noNetworkDialog showing");
				noNetworkDialog.show();
			}
			else
			{
				runOnUiThread(new Runnable() {
					public void run() {
						tabHost.setCurrentTab(0);
					}
				});
			}
		}
	}
	private class RefresherDaemon extends TimerTask {
		private Runnable runnable = new Runnable() {
			
			public void run() {
				new RefreshLists().execute();
			}
		};
		
		@Override
		public void run() {
			handler.post(runnable);
		}
	}
	private void handleRefresherDaemon() {
        if(Settings.AUTO_REFRESH) {
        	if(refresherDaemon == null)
        		startRefresherDaemon();
        }
        else {
        	if(refresherDaemon != null)
        		killRefresherDaemon();
        }
	}
	private void startRefresherDaemon() {
		refresherDaemon = new Timer(); refresherDaemon.schedule(new RefresherDaemon(), Settings.AUTO_REFRESH_TIME_INTERVAL*1000, Settings.AUTO_REFRESH_TIME_INTERVAL*1000);
	}
	private void killRefresherDaemon() {
		refresherDaemon.cancel();
		refresherDaemon = null;
	}

}
